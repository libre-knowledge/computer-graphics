# 電腦產圖<br>Computer graphics

經由電腦輔助以產生影像或圖像

<https://gitlab.com/libre-knowledge/computer-graphics>  
[![GitLab CI 持續整合流程狀態標章](https://gitlab.com/libre-knowledge/computer-graphics/badges/main/pipeline.svg?ignore_skipped=true "點擊查看 GitLab CI 持續整合流程的運行狀態")](https://gitlab.com/libre-knowledge/computer-graphics/-/commits/main) [![「檢查專案中的潛在問題」GitHub Actions 作業流程狀態標章](https://github.com/libre-knowledge/computer-graphics/actions/workflows/check-potential-problems.yml/badge.svg "本專案使用 GitHub Actions 自動化檢查專案中的潛在問題")](https://github.com/libre-knowledge/computer-graphics/actions/workflows/check-potential-problems.yml) [![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white "本專案使用 pre-commit 檢查專案中的潛在問題")](https://github.com/pre-commit/pre-commit) [![REUSE 規範遵從狀態標章](https://api.reuse.software/badge/gitlab.com/libre-knowledge/computer-graphics "本專案遵從 REUSE 規範降低軟體授權合規成本")](https://api.reuse.software/info/gitlab.com/libre-knowledge/computer-graphics)

## 基本概念

以下列舉本主題相關的基本概念說明資源：

* [計算機圖形 - 維基百科，自由的百科全書](https://zh.wikipedia.org/wiki/%E8%AE%A1%E7%AE%97%E6%9C%BA%E5%9B%BE%E5%BD%A2)
* [Computer graphics - Wikipedia](https://en.wikipedia.org/wiki/Computer_graphics)

## 子主題

以下列舉本主題相關的主題：

* [動態畫面更新率 Variable Refresh Rate(VRR)](https://gitlab.com/libre-knowledge/variable-refresh-rate)  
  使螢幕得以使用與顯示界面相同的速度更新畫面，避免畫面撕裂

## 參考資料

以下列舉撰寫本主題內容時所參考的第三方資源：

* [計算機圖形 - 維基百科，自由的百科全書](https://zh.wikipedia.org/wiki/%E8%AE%A1%E7%AE%97%E6%9C%BA%E5%9B%BE%E5%BD%A2)  
  [Computer graphics - Wikipedia](https://en.wikipedia.org/wiki/Computer_graphics)  
  說明本主題的基本概念

---

本主題為[自由知識協作平台](https://gitlab.com/libre-knowledge/libre-knowledge)的一部分，除部份特別標註之經合理使用(fair use)原則使用的內容外允許公眾於授權範圍內自由使用

如有任何問題，歡迎於本主題的[議題追蹤系統](https://gitlab.com/libre-knowledge/computer-graphics/-/issues)創建新議題反饋
